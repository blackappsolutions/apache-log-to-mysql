# Pump multiple existing apache logs to MySQL with Perl

## Preface
This script was heavily inspired by http://www.shayanderson.com/perl/multiple-apache-logs-to-mysql-database-table-with-perl-script.htm

### Prerequisite
* Perl and mysql was installed (with homebrew on macOS)
* You have apache logs in the following format:
  ```
  10.10.76.60 - - [22/Feb/2020:03:32:36 +0000] "GET /en/iaaspaas-explanatory-video HTTP/1.1" 200 17250 "https://www.google.com/search?hl=en&q=testing" "1'\""
  10.10.76.61 - - [22/Feb/2020:02:59:41 +0000] "GET / HTTP/1.1" 302 - "() { Referer; }; echo -e \"Content-Type: text/plain\\n\"; echo -e \"\\0141\\0143\\0165\\0156\\0145\\0164\\0151\\0170\\0163\\0150\\0145\\0154\\0154\\0163\\0150\\0157\\0143\\0153\"" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"     
  ```
* If you wan't to extract parts of a huge log file, then this hints might be helpful:
  ```                               
  # grep the starting line number
  grep -n -e '10.10.76.60 - - \[22/Feb/2020:02:59:39 +0000\] "GET /robots.txt HTTP/1.1"' access.log
  42403:10.10.76.60 - - [22/Feb/2020:02:59:39 +0000] "GET /robots.txt HTTP/1.1" 200 146 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
  
  # grep the terminating line number
  grep -n -e '10.10.76.61 - - \[22/Feb/2020:09:00:18 +0000\] "GET /d/ajaxaction/de/interactive/1786/1786 HTTP/1.1"' access.log
  660812:10.10.76.61 - - [22/Feb/2020:09:00:18 +0000] "GET /d/ajaxaction/de/interactive/1786/1786 HTTP/1.1" 302 - "https://www.google.com/search?hl=en&q=testing" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
  
  # cut the portion between those lines  
  sed -n '42403,660812p' access.log > access.log.part
  ```   
  
### Installation
* Install perl's mysql lib: `cpan install DBD::mysql`
* If you encounter installation problems (see https://stackoverflow.com/a/56196487), then pimp `/usr/local/opt/mysql/bin/mysql_config` like this: 
  <br/>Before:
  ```                                                                                     
  libs="-L$pkglibdir"
  libs="$libs -lmysqlclient -lssl -lcrypto"
  ``` 
  After:
  ```                                                                                     
  libs="-L$pkglibdir"
  libs="$libs -lmysqlclient -lssl -lcrypto"
  libs="$libs -L/usr/local/opt/openssl/lib"
  ```                                     
  (Make sure `/usr/local/opt/openssl/lib` exists.)
* Make sure mysql is running, then create the database and user: 
  ```
  $ mysql --user=root
  mysql> CREATE SCHEMA apache_logs CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
  mysql> GRANT ALL PRIVILEGES ON apache_logs.* TO 'apache_logs'@'localhost' IDENTIFIED BY 'apache_logs';
  mysql> commit;
  ```

### Execution
This call 
```
$ ./apachelog2mysql.pl apache-access.log "localhost:apache_logs:apache_logs:apache_logs"
```               
will create the following table
```
mysql> use apache_logs;
Database changed
mysql> describe apachelog;
+-----------+------------------+------+-----+---------+----------------+
| Field     | Type             | Null | Key | Default | Extra          |
+-----------+------------------+------+-----+---------+----------------+
| id        | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| log       | varchar(255)     | NO   |     | NULL    |                |
| created   | varchar(255)     | YES  |     | NULL    |                |
| retcode   | int(4)           | YES  |     | NULL    |                |
| method    | varchar(50)      | YES  |     | NULL    |                |
| url       | varchar(62500)   | YES  |     | NULL    |                |
| referrer  | varchar(1024)    | YES  |     | NULL    |                |
| useragent | varchar(1024)    | YES  |     | NULL    |                |
+-----------+------------------+------+-----+---------+----------------+
8 rows in set (0,00 sec)
```
### Analysis
Now you can start analyzing the data with sql. So maybe you want the return number of calls which have had the return code 500?
```sql
select count(*) from apachelog where retcode = 500;
```                                                
or you want to know what return codes at all occurred:
```sql
select distinct retcode from apachelog;
```
