#!/usr/local/Cellar/perl/5.26.1/bin/perl

############################################################################################################
# apachelog2mysql.pl
#
# Apache log(s) to MySQL table
#
# Usage: ./{file} {log path(s)} {mysql db credentials}
# Example: ./{file} "/dir1/log1, /dir2/log2" "db_host:db_database:db_username:db_password"
#
# @name apachelog2mysql
# @version 1.0
# @author Shay Anderson 01.13
# @copyright 2013 ShayAnderson.com <http://www.shayanderson.com>
# @license MIT License <http://www.opensource.org/licenses/mit-license.php>
# @link http://www.shayanderson.com/perl/multiple-apache-logs-to-mysql-database-table-with-perl-script.htm
############################################################################################################
use strict;
use warnings;
use DBI;

my %conf = (
  "table_name" => "apachelog"
);

if(@ARGV < 2)
{
  print "Usage: " . __FILE__ . " {log path(s)} {mysql db credentials}\n"
    . "Example: " . __FILE__ . " \"/dir1/log1, /dir2/log2\" "
    . "\"db_host:db_database:db_username:db_password\"\n\n";
  exit;
}

my ($arg_logs) = $ARGV[0];
my ($arg_db) = $ARGV[1];

my @logs = split(',', $arg_logs);
my @db = split(':', $arg_db);
if(@db < 4)
{
  print "Failed to continue, invalid DB configuration settings\n\n";
  exit;
}

my $lines = 0;
foreach(@logs)
{
  my $log = $_;
  $log =~ s/^\s+//; # rem lead spaces
  $log =~ s/\s+$//; # rem tail spaces
  processLog($log);
}

print "\nTotal log entries inserted: " . $lines . "\n\n";

sub processLog
{
  my $log = $_[0];
  print "\nProcessing log \"" . $log . "\"...\n";
  
  open my $data, $log or die "\nFailed to open log file: \"" . $log . "\"\n\n";
  
  my $dbc = DBI->connect('DBI:mysql:' . $db[1] . ':' . $db[0], 
    $db[2], $db[3], { RaiseError => 1 })
    or die "\nFailed to connect to database \"" . $db[1] . "\"";
  
  initDB($dbc);
  
  while(my $line = <$data>)
  {
    chomp($line);
    $line =~ s/'/\\'/g; # ' to \' for db
    if(length($line) > 1)
    {
      my ($created, $method, $url, $retcode, $referrer, $useragent) = $line =~ m/^.*\[([^\]]+)\] \"([^\s]+) ([^\s]+) HTTP\/1.1\" ([^\s]+).* \"(.*)\" \"(.*)\"/;
      my $http_error_code = $retcode // 0;
      my $q = "INSERT INTO " . $conf{'table_name'}
        . "(log, created, retcode, method, url, referrer, useragent) VALUES('"
        . $log . "', '" . $created . "', " . $http_error_code . ", '" . $method . "','" . $url
        . "','" . $referrer . "','" . $useragent . "');";
      my $dbs = $dbc->prepare($q);
      $lines = $lines + $dbs->execute() or print "\nFailed to insert '" . $q . "' " . $line . "\"\n";
    }
  }
  
  $dbc->disconnect;
  
  close $data;
  close $log;
  
  # flush log file
  `echo '' > $log`;
}

my $is_db_ready = 0;
sub initDB
{
  if(!$is_db_ready)
  {
    print "Initializing database...\n";
    
    my $dbc = $_[0];
    
    my $dbs = $dbc->prepare("
      CREATE TABLE IF NOT EXISTS `" . $conf{'table_name'} . "` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `log` varchar(255) NOT NULL,
        `created` varchar(255) DEFAULT NULL,
        `retcode` int(4) DEFAULT NULL,
        `method` varchar(50) DEFAULT NULL,
        `url` varchar(62500) DEFAULT NULL,
        `referrer` varchar(1024) DEFAULT NULL,
        `useragent` varchar(1024) DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
    ");
    # Removed:
    #`stored` timestamp NULL DEFAULT NULL
    #UNIQUE KEY `entry_unique` (`log`,`type`,`mhash`)
    
    $dbs->execute() or die "\nFailed to execute create table \"" 
      . $conf{'table_name'} . "\" query\n";
    
    $is_db_ready = 1;
  }
}
